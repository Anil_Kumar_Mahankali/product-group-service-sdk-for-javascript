import {inject} from 'aurelia-dependency-injection';
import ProductGroupServiceSdkConfig from './productGroupServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import ProductGroupSynopsisView from './productGroupSynopsisView';

@inject(ProductGroupServiceSdkConfig, HttpClient)
class ListProductGroupsFeature {

    _config:ProductGroupServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ProductGroupServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Lists all product groups
     * @param {string} accessToken
     * @returns {Promise.<ProductGroupSynopsisView[]>}
     */
    execute(accessToken:string):Promise<Array> {

        return this._httpClient
                .createRequest('product-groups')
                .asGet()
                .withBaseUrl(this._config.precorConnectApiBaseUrl)
                .withHeader('Authorization', `Bearer ${accessToken}`)
                .send()
                .then(response => {

                    return Array.from(
                            response.content,
                            contentItem =>
                                new ProductGroupSynopsisView(
                                    contentItem.id,
                                    contentItem.name
                                )
                        );
                });
    }
}

export default ListProductGroupsFeature;
