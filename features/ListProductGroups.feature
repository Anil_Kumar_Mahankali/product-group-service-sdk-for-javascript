Feature: List Product Groups
  Lists all product groups

  Scenario: Success
    Given I provide a valid accessToken
    When I execute listProductGroups
    Then all product groups in the product-group-service are returned